/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#ifndef PROJECT2_ZOO_HPP
#define PROJECT2_ZOO_HPP

#include "penguin.hpp"
#include "turtle.hpp"
#include "tiger.hpp"

class zoo {
public:
    //constructor creates objects to fill the arrays
    zoo() {
        for (int i = 0; i < penguinArraySize; i++) {
            penguin *newPenguin = new penguin();
            penguinArray[i] = newPenguin;
            delete newPenguin;
        }

        for (int i = 0; i < tigerArraySize; i++) {
            tiger *newTiger = new tiger();
            tigerArray[i] = newTiger;
            delete newTiger;
        }

        for (int i = 0; i < turtleArraySize; i++) {
            turtle *newTurtle = new turtle();
            turtleArray[i] = newTurtle;
            delete newTurtle;
        }
    }

    //destructor
    ~zoo() = default;

    void ageAnimals();

    void feedAnimals();

    void randomEvent();

    //get functions
    int getMoney();

    int getDayCount();

    int getTigerCount();

    int getPenguinCount();

    int getTurtleCount();

    bool getFirstTimeBuyingAnimal();

    tiger *getTigerArray();

    turtle *getTurtleArray();

    penguin *getPenguinArray();

    //set functions
    void setDayCount();

    void setMoney(int);

    void setTigerArray(tiger);

    void setPenguinArray(penguin);

    void setTurtleArray(turtle);

    void setFirstTimeBuyingAnimal(bool);

    void setTigerCount(int);

    void setPenguinCount(int);

    void setTurtleCount(int);

private:
    int tigerCount = 0;
    int penguinCount = 0;
    int turtleCount = 0;
    int money = 25000;
    int dayCount = 1;
    int penguinArraySize = 10;
    int turtleArraySize = 10;
    int tigerArraySize = 10;
    bool firstTimeBuyingAnimal = true;

    //creates arrays for each animal object type
    penguin *penguinArray = new penguin[10];
    turtle *turtleArray = new turtle[10];
    tiger *tigerArray = new tiger[10];
};


#endif //PROJECT2_ZOO_HPP
