/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#include <iostream>
#include <limits>
#include "game.hpp"
#include "zoo.hpp"
#include "tiger.hpp"
#include "penguin.hpp"
#include "turtle.hpp"

//get functions
std::string game::getGameStatus() {
    return gameStatus;
}

std::string game::getChoice() {
    return choice;
}

//set functions
void game::setChoice(std::string c) {
    choice = c;
}

//input validation
bool isDigits(std::string &str) {
    return str.find_first_not_of("0123456789") == std::string::npos;
}

//menu function lets user choose to play the game or not
void game::menu() {
    bool menuQuit = false;
    bool goodInput = false;
    int choiceInt;

    while (!menuQuit) {
        while (!goodInput) {
            //tells the user to pick an option
            std::cout << "What would you like to do?\n"
                      << "1. Play game\n"
                      << "2. Exit game\n";

            //takes in the option that the user chooses
            std::cin >> std::skipws >> choice;

            //incorrect answer
            if (!isDigits(choice) || (choice != "1" && choice != "2")) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << std::endl;
                goodInput = false;
                std::cout << "Please enter a valid input\n";
                break;
            }

            //correct answer
            if (choice == "1" || choice == "2") {
                if (choice == "1") {
                    choiceInt = 1;
                }
                if (choice == "2") {
                    choiceInt = 2;
                }
                goodInput = true;
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                break;
            }
        }
        switch (choiceInt) {
            case 1: {
                std::cout << "Starting game...\n";
                menuQuit = true;
                break;
            }

            case 2: {
                std::cout << "Quitting...\n";
                return;
            }

            default: {
                break;
            }
        }
    }

}

//lets user choose which animals to buy
void game::buyAnimal(zoo *zoo1) {
    bool menuQuit = false;
    bool goodInput = false;
    int buyAnimal = 0;

    //this is the purchase animal menu if the user has bought animals already
    if (!(zoo1->getFirstTimeBuyingAnimal())) {
        while (!menuQuit && (zoo1->getMoney() > 0)) {
            while (!goodInput) {
                //tells the user to pick an option
                std::cout << "Would you like to purchase an adult virtual animal? Your current balance is: $"
                          << zoo1->getMoney() << " (y/n)\n";

                //takes in the option that the user chooses
                std::cin >> purchaseAnimal;

                if (purchaseAnimal == "y") {
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    goodInput = true;
                    menuQuit = false;
                    buyAnimal = 1;
                    break;
                }
                if (purchaseAnimal == "n") {
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    goodInput = true;
                    menuQuit = true;
                    buyAnimal = 2;
                    break;
                }
                //incorrect answer
                if (purchaseAnimal != "n" && purchaseAnimal != "y") {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    std::cout << std::endl;
                    goodInput = false;
                    std::cout << "Please enter a valid input\n";
                }

            }
            //asks the user which animal they would like to buy
            switch (buyAnimal) {
                case 1: {
                    goodInput = false;
                    while (!goodInput) {
                        std::cout << "Which virtual animal type would you like to purchase?\n"
                                  << "1. Tiger ($10,000) \n"
                                  << "2. Penguin ($1,000) \n"
                                  << "3. Turtle ($100) \n";

                        std::cin >> purchase;

                        //input validation
                        if (purchase >= 1 && purchase <= 3) {
                            goodInput = true;
                            break;
                        }

                        if (purchase != 1 || purchase != 2 || purchase != 3) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << std::endl;
                            goodInput = false;
                            std::cout << "Please enter a valid input\n";
                        }
                    }

                    while (!menuQuit) {
                        switch (purchase) {
                            //purchase tiger
                            case 1: {
                                std::cout << "Purchasing...\n";

                                auto *newTiger = new tiger();
                                newTiger->setAnimalAge(3);
                                zoo1->setTigerArray(*newTiger);
                                //get tiger array, then set age
                                int cost = newTiger->getAnimalCost();
                                zoo1->setMoney((zoo1->getMoney() - cost));
                                goodInput = true;
                                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                                menuQuit = true;
                                delete newTiger;
                                break;
                            }
                                //purchase penguin
                            case 2: {
                                std::cout << "Purchasing...\n";

                                penguin *newPenguin = new penguin();
                                newPenguin->setAnimalAge(3);
                                zoo1->setPenguinArray(*newPenguin);
                                int cost = newPenguin->getAnimalCost();
                                zoo1->setMoney((zoo1->getMoney() - cost));
                                goodInput = true;
                                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                                menuQuit = true;
                                delete newPenguin;
                                break;
                            }

                            case 3: {
                                //purchase turtle
                                std::cout << "Purchasing...\n";

                                turtle *newTurtle = new turtle();
                                newTurtle->setAnimalAge(3);
                                zoo1->setTurtleArray(*newTurtle);
                                int cost = newTurtle->getAnimalCost();
                                zoo1->setMoney((zoo1->getMoney() - cost));
                                goodInput = true;
                                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                                zoo1->setFirstTimeBuyingAnimal(false);
                                menuQuit = true;
                                delete newTurtle;
                                break;
                            }
                        }
                    }
                }
                case 2: {
                    break;
                }
            }
        }
    }

    //this is the purchase animal menu if the user has not bought any animals yet
    if (zoo1->getFirstTimeBuyingAnimal()) {
        while (!menuQuit && (zoo1->getMoney() >= 0)) {
            while (!goodInput) {
                //tells the user to pick an option
                std::cout
                        << "How many virtual tigers would you like to purchase? (1 or 2) Your current balance is: $"
                        << zoo1->getMoney() << "\n";

                //takes in the option that the user chooses
                std::cin >> purchase;

                //correct answer
                if (purchase >= 1 && purchase <= 2) {
                    std::cout << "Purchasing...\n";

                    for (int i = 0; i < purchase; i++) {
                        tiger *newTiger = new tiger();
                        zoo1->setTigerArray(*newTiger);
                        //get tiger array, then set age
                        int cost = newTiger->getAnimalCost();
                        zoo1->setMoney((zoo1->getMoney() - cost));
                        delete newTiger;
                    }
                    goodInput = true;
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }

                //incorrect answer
                if (purchase != 1 && purchase != 2) {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    std::cout << std::endl;
                    goodInput = false;
                    std::cout << "Please enter a valid input\n";
                }
            }

            goodInput = false;
            while (!goodInput) {
                //tells the user to pick an option
                std::cout
                        << "How many virtual penguins would you like to purchase? (1 or 2) Your current balance is: $"
                        << zoo1->getMoney() << "\n";

                //takes in the option that the user chooses
                std::cin >> purchase;

                //correct answer
                if (purchase >= 1 && purchase <= 2) {
                    std::cout << "Purchasing...\n";

                    for (int i = 0; i < purchase; i++) {
                        penguin *newPenguin = new penguin();
                        zoo1->setPenguinArray(*newPenguin);
                        int cost = newPenguin->getAnimalCost();
                        zoo1->setMoney((zoo1->getMoney() - cost));
                        delete newPenguin;
                    }
                    goodInput = true;
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }

                //incorrect answer
                if (purchase != 1 && purchase != 2) {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    std::cout << std::endl;
                    goodInput = false;
                    std::cout << "Please enter a valid input\n";
                }
            }
            goodInput = false;
            while (!goodInput) {
                //tells the user to pick an option
                std::cout
                        << "How many virtual turtles would you like to purchase? (1 or 2) Your current balance is: $"
                        << zoo1->getMoney() << "\n";

                //takes in the option that the user chooses
                std::cin >> purchase;

                //correct answer
                if (purchase >= 1 && purchase <= 2) {
                    std::cout << "Purchasing...\n";

                    for (int i = 0; i < purchase; i++) {
                        turtle *newTurtle = new turtle();
                        zoo1->setTurtleArray(*newTurtle);
                        int cost = newTurtle->getAnimalCost();
                        zoo1->setMoney((zoo1->getMoney() - cost));
                        delete newTurtle;
                    }
                    goodInput = true;
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    zoo1->setFirstTimeBuyingAnimal(false);
                    menuQuit = true;
                }

                //incorrect answer
                if (purchase != 1 && purchase != 2) {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    std::cout << std::endl;
                    goodInput = false;
                    std::cout << "Please enter a valid input\n";
                }
            }
        }
    }
}

//gets the user to buy their initial animals for their zoo
void game::setupGame(zoo *zoo1) {
    std::cout << "To start the game you must buy at least 3 different animal types. \n";
    buyAnimal(zoo1);
}

//makes the user take a turn
void game::takeTurn(zoo *zoo1) {
    int preTurnMoney;
    int profit;
    int incomeFromAnimals;

    //pre turn money is used to calculate profit
    preTurnMoney = zoo1->getMoney();

    //ages the animals since a day has now passed
    zoo1->ageAnimals();

    //feeds the animals since a day has now passed
    zoo1->feedAnimals();

    //makes a random event happen
    zoo1->randomEvent();

    //income from animals is based on each animals return
    incomeFromAnimals = ((zoo1->getTigerCount() * 2000) + (zoo1->getPenguinCount() * 100) +
                         (zoo1->getTurtleCount() * 5));

    //adds the income from animals to the money the user has
    zoo1->setMoney(zoo1->getMoney() + incomeFromAnimals);

    //calculates profit
    profit = zoo1->getMoney() - preTurnMoney;

    //shows the user their stats
    std::cout << "Number of tigers: " << zoo1->getTigerCount() << "\n";
    std::cout << "Number of penguins: " << zoo1->getPenguinCount() << "\n";
    std::cout << "Number of turtles: " << zoo1->getTurtleCount() << "\n";
    std::cout << "You have $" << zoo1->getMoney() << ".\n";
    std::cout << "You made $" << profit << " today!\n";

    //lets the user choose to buy an animal
    buyAnimal(zoo1);

    //end of turn
    //triggers if the user runs out of money
    if (zoo1->getMoney() < 0) {
        endGame(zoo1);
        std::cout << "You ran out of money! You lose...\n";
        gameStatus = "n";
    }
    //lets the user keep playing if they still have money
    if (zoo1->getMoney() > 0) {
        std::cout << "Would you like to keep playing? (y/n)\n";
        std::cin >> gameStatus;

        //ends the game if the user chooses to
        if (gameStatus == "n") {
            endGame(zoo1);
        }
    }
    //incorrect answer
    if (gameStatus != "n" && gameStatus != "y") {

        while (gameStatus != "n" && gameStatus != "y") {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << std::endl;
            std::cout << "Please enter a valid input\n";
            std::cout << "Would you like to keep playing? (y/n)\n";
            std::cin >> gameStatus;
        }
    }
}

//endGame being called deletes dynamic arrays and objects
void game::endGame(zoo *zoo1) {
    delete[] zoo1->getTigerArray();
    zoo1->setTigerCount(0);
    zoo1->setTigerArray(nullptr);
    delete[] zoo1->getTurtleArray();
    zoo1->setTurtleCount(0);
    zoo1->setTurtleArray(nullptr);
    delete[] zoo1->getPenguinArray();
    zoo1->setPenguinCount(0);
    zoo1->setPenguinArray(nullptr);
}