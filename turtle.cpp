/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#include "turtle.hpp"

//get functions
int turtle::getAnimalCost() {
    return cost;
}

int turtle::getAnimalAge() {
    return age;
}

//set functions
void turtle::setAnimalAge(int aa) {
    age += aa;
}

//constructor
turtle::turtle(turtle *turtle) {

}

