/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#ifndef PROJECT2_ANIMAL_HPP
#define PROJECT2_ANIMAL_HPP

class animal {
public:
    //default constructor
    animal() = default;

    //destructor
    virtual ~animal() = default;

    //get functions
    virtual int getAnimalAge();

    virtual int getAnimalCost();

    virtual int getAnimalBaseFoodCost();

    //set functions
    virtual void setAnimalAge(int);

private:
    int age{};
    int cost{};
    int numberOfBabies{};
    int baseFoodCost{};
    int payoff{};

};

#endif //PROJECT2_ANIMAL_HPP
