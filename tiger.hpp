/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#ifndef PROJECT2_TIGER_HPP
#define PROJECT2_TIGER_HPP

#include "animal.hpp"

class tiger : public animal {
public:
    //default constructor
    tiger() = default;

    //destructor
    ~tiger() = default;

    //constructor
    tiger(tiger *tiger);

    //set functions
    void setAnimalAge(int);

    //get functions
    int getAnimalAge();

    int getAnimalCost();

private:
    int age = 1;
    int cost = 10000;
    int numberOfBabies = 1;
    int baseFoodCost = 50;
    int payoff = 2000;
};


#endif //PROJECT2_TIGER_HPP
