/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#ifndef PROJECT2_PENGUIN_HPP
#define PROJECT2_PENGUIN_HPP

#include "animal.hpp"

class penguin : public animal {
public:
    //default constructor
    penguin() = default;

    //destructor
    ~penguin() = default;

    //constructor
    penguin(penguin *penguin);

    //set functions
    void setAnimalAge(int);

    //get functions
    int getAnimalCost();

    int getAnimalAge();


private:
    int age = 1;
    int cost = 1000;
    int numberOfBabies = 5;
    int baseFoodCost = 10;
    int payoff = 100;
};


#endif //PROJECT2_PENGUIN_HPP
