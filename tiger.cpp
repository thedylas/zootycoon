/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#include "tiger.hpp"
#include "zoo.hpp"

//set functions
void tiger::setAnimalAge(int aa) {
    age += aa;
}

//get functions
int tiger::getAnimalCost() {
    return cost;
}

int tiger::getAnimalAge() {
    return age;
}

//constructor
tiger::tiger(tiger *tiger) {

}

