/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#include "zoo.hpp"
#include <iostream>

//increments the age of the animals in the zoo
void zoo::ageAnimals() {
    for (int i = 0; i < tigerCount; i++) {
        tigerArray[i].setAnimalAge(1);
    }
    for (int i = 0; i < turtleCount; i++) {
        turtleArray[i].setAnimalAge(1);
    }
    for (int i = 0; i < penguinCount; i++) {
        penguinArray[i].setAnimalAge(1);
    }
}

//get functions
tiger *zoo::getTigerArray() {
    return tigerArray;
}

turtle *zoo::getTurtleArray() {
    return turtleArray;
}

penguin *zoo::getPenguinArray() {
    return penguinArray;
}

bool zoo::getFirstTimeBuyingAnimal() {
    return firstTimeBuyingAnimal;
};

int zoo::getMoney() {
    return money;
}

int zoo::getDayCount() {
    return dayCount;
}

int zoo::getTigerCount() {
    return tigerCount;
}

int zoo::getPenguinCount() {
    return penguinCount;
}

int zoo::getTurtleCount() {
    return turtleCount;
}

//set functions
void zoo::setTigerCount(int tc) {
    tigerCount = tc;
}

void zoo::setPenguinCount(int pc) {
    penguinCount = pc;
}

void zoo::setTurtleCount(int tc) {
    turtleCount = tc;
}

void zoo::setFirstTimeBuyingAnimal(bool ftba) {
    firstTimeBuyingAnimal = ftba;
}

void zoo::setDayCount() {
    dayCount++;
}

void zoo::setMoney(int m) {
    money = m;
}

void zoo::setTigerArray(tiger newTiger) {
    if ((tigerCount + 1) >= tigerArraySize) {
        tiger *temp;
        temp = new tiger[(tigerArraySize * 2)];
        for (int i = 0; i < tigerArraySize; i++) {
            temp[i] = tigerArray[i];
        }
        tigerArraySize *= 2;
        delete[] tigerArray;
        tigerArray = temp;
    }
    tigerArray[tigerCount] = newTiger;
    tigerCount++;
}

void zoo::setTurtleArray(turtle newTurtle) {
    if ((turtleCount + 1) >= turtleArraySize) {
        turtle *temp;
        temp = new turtle[(turtleArraySize * 2)];
        for (int i = 0; i < turtleArraySize; i++) {
            temp[i] = turtleArray[i];
        }
        turtleArraySize *= 2;
        delete[] turtleArray;
        turtleArray = temp;
    }
    turtleArray[turtleCount] = newTurtle;
    turtleCount++;
}

void zoo::setPenguinArray(penguin newPenguin) {
    if ((penguinCount + 1) >= penguinArraySize) {
        penguin *temp;
        temp = new penguin[(penguinArraySize * 2)];
        for (int i = 0; i < penguinArraySize; i++) {
            temp[i] = penguinArray[i];
        }
        penguinArraySize *= 2;
        delete[] penguinArray;
        penguinArray = temp;
    }
    penguinArray[penguinCount] = newPenguin;
    penguinCount++;
}

//feeds the animals in the zoo and subtracts the cost
void zoo::feedAnimals() {
    int foodCost;
    foodCost = (tigerCount * tigerArray[0].getAnimalBaseFoodCost()) \
 + (turtleCount * turtleArray[0].getAnimalBaseFoodCost()) \
 + (penguinCount * penguinArray[0].getAnimalBaseFoodCost());
    money -= foodCost;
}

//1 random event occurs every day
void zoo::randomEvent() {

    //this variable determines the random event that occurs
    int event;
    event = rand() % 4 + 1;
    switch (event) {
        //this event kills a random animal
        case 1: {
            int unluckyAnimal;
            unluckyAnimal = rand() % 3 + 1;
            switch (unluckyAnimal) {
                case 1: {
                    if (tigerCount >= 1) {
                        tigerArray[(tigerCount - 1)] = nullptr;
                        tigerCount--;
                        std::cout << "A sickness occurs to an animal in the zoo! You have lost a tiger.\n";
                        break;
                    }
                    if (tigerCount <= 0) {
                    }
                }
                case 2: {
                    if (penguinCount >= 1) {
                        penguinArray[(penguinCount - 1)] = nullptr;
                        penguinCount--;
                        std::cout << "A sickness occurs to an animal in the zoo! You have lost a penguin.\n";
                        break;
                    }
                    if (penguinCount <= 0) {
                    }

                }
                case 3: {
                    if (turtleCount >= 1) {
                        turtleArray[(turtleCount - 1)] = nullptr;
                        turtleCount--;
                        std::cout << "A sickness occurs to an animal in the zoo! You have lost a turtle.\n";
                        break;
                    }
                    if (turtleCount <= 0) {
                    }
                }
            }
            break;
        }
            //this event gives the user a bonus amount of money
        case 2: {
            int bonusAmount;
            int totalBonus;
            bonusAmount = rand() % 250 + 250;

            //the number of tigers in the zoo is a multiplier on the random number generated
            totalBonus = bonusAmount * tigerCount;

            //adds the bonus to the money the user has
            money += totalBonus;
            std::cout << "A boom in zoo attendance occurs! You generated an extra $" << totalBonus << " today!\n";
            break;
        }
            //this event gives the user baby animals
        case 3: {
            //this variable is used for the loop while looking for an adult animal
            bool adultFound = false;
            while (!adultFound) {
                int luckyAnimal;
                luckyAnimal = rand() % 3 + 1;
                switch (luckyAnimal) {

                    //this switch statement determines which animal will have babies
                    case 1: {
                        bool oldEnoughAnimal = false;
                        for (int i = 0; i < tigerCount; i++) {
                            if (tigerArray[i].getAnimalAge() >= 3) {
                                auto *newTiger = new tiger();
                                setTigerArray(*newTiger);
                                tigerArray[tigerCount].setAnimalAge(0);
                                oldEnoughAnimal = true;
                                std::cout << "A baby animal is born! You have a new baby tiger!\n";
                                adultFound = true;
                                delete newTiger;
                                break;
                            }
                        }
                        if (!oldEnoughAnimal) {
                        }
                        //ends the loop if there is an adult
                        if (adultFound) {
                            break;
                        }
                    }
                    case 2: {
                        bool oldEnoughAnimal = false;
                        for (int i = 0; i < penguinCount; i++) {
                            if (penguinArray[i].getAnimalAge() >= 3) {
                                for (int i = 0; i < 5; i++) {
                                    penguin *newPenguin = new penguin();
                                    setPenguinArray(*newPenguin);
                                    penguinArray[penguinCount].setAnimalAge(0);
                                    oldEnoughAnimal = true;
                                    adultFound = true;
                                    delete newPenguin;
                                }
                                std::cout << "Baby animals are born!  You have 5 new baby penguins!\n";
                                break;
                            }
                        }
                        if (!oldEnoughAnimal) {
                        }
                        if (adultFound) {
                            break;
                        }
                    }
                    case 3: {
                        bool oldEnoughAnimal = false;
                        for (int i = 0; i < turtleCount; i++) {
                            if (turtleArray[i].getAnimalAge() >= 3) {
                                for (int i = 0; i < 10; i++) {
                                    turtle *newTurtle = new turtle();
                                    setTurtleArray(*newTurtle);
                                    turtleArray[turtleCount].setAnimalAge(0);
                                    oldEnoughAnimal = true;
                                    adultFound = true;
                                    delete newTurtle;
                                }
                                std::cout << "Baby animals are born!  You have 10 new baby turtles!\n";
                                break;
                            }
                        }
                        if (!oldEnoughAnimal) {
                            break;
                        }
                        if (adultFound) {
                            break;
                        }
                    }
                }
            }
            break;
        }
            //this 4th random event makes nothing happen
        case 4: {
            std::cout << "Nothing happens...\n";
            break;
        }
    }
}