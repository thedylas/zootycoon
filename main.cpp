/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#include <iostream>
#include "game.hpp"

int main() {

    //creates game and zoo objects
    game game1;
    zoo zoo1;

    std::cout << "\nWelcome to zoo tycoon!!! Your virtual adventure awaits!!\n\n";

    //opens the initial menu asking the user if they want to play the game
    game1.menu();

    //if the user wants to play the game then the game gets setup
    while (game1.getChoice() == "1") {
        game1.setupGame(&zoo1);

        //each iteration of this loop is another day
        while (true) {
            std::cout << "Day: " << zoo1.getDayCount() << "\n";

            //day count is iterated
            zoo1.setDayCount();

            //this function takes the turn for the user
            game1.takeTurn(&zoo1);

            //if the user selects that they no longer want to play the game then the loop is terminated
            if (game1.getGameStatus() == "n") {
                std::cout << "Quitting...\n";
                game1.setChoice("2");
                break;
            }
        }
    }
}