/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#ifndef PROJECT2_GAME_HPP
#define PROJECT2_GAME_HPP

#include "zoo.hpp"

class game {
public:
    //uses default constructor
    game() = default;

    //destructor
    ~game() = default;

    void menu();

    void setupGame(zoo *);

    void takeTurn(zoo *);

    void buyAnimal(zoo *);

    void endGame(zoo *);

    //get functions
    std::string getGameStatus();

    std::string getChoice();

    //set functions
    void setChoice(std::string);

private:
    std::string gameStatus{};
    std::string purchaseAnimal{};
    std::string choice{};
    int purchase{};

};

#endif //PROJECT2_GAME_HPP
