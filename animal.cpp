/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#include "animal.hpp"

//get functions
int animal::getAnimalAge() {
    return age;
}

int animal::getAnimalCost() {
    return cost;
}

int animal::getAnimalBaseFoodCost() {
    return baseFoodCost;
}

//set functions
void animal::setAnimalAge(int aa) {
    age += aa;
}

