OBJS	= main.o animal.o zoo.o game.o penguin.o tiger.o turtle.o
SOURCE	= main.cpp animal.cpp zoo.cpp game.cpp penguin.cpp tiger.cpp turtle.cpp
HEADER	= animal.hpp zoo.hpp game.hpp penguin.hpp tiger.hpp turtle.hpp
OUT	= project2
CC	 = g++
FLAGS	 = -std=c++11 -g -c -Wall
LFLAGS	 = 

all: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) $(LFLAGS)

main.o: main.cpp
	$(CC) $(FLAGS) main.cpp 

animal.o: animal.cpp
	$(CC) $(FLAGS) animal.cpp 

zoo.o: zoo.cpp
	$(CC) $(FLAGS) zoo.cpp 

game.o: game.cpp
	$(CC) $(FLAGS) game.cpp 

penguin.o: penguin.cpp
	$(CC) $(FLAGS) penguin.cpp 

tiger.o: tiger.cpp
	$(CC) $(FLAGS) tiger.cpp 

turtle.o: turtle.cpp
	$(CC) $(FLAGS) turtle.cpp 


clean:
	rm -f $(OBJS) $(OUT)
