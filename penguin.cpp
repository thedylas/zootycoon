/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#include "penguin.hpp"

//get functions
int penguin::getAnimalCost() {
    return cost;
}

int penguin::getAnimalAge() {
    return age;
}

//set functions
void penguin::setAnimalAge(int aa) {
    age += aa;
}

//constructor
penguin::penguin(penguin *penguin) {

}

