/*********************************************************************
 ** Program name: project2
 ** Author: David Anderson
 ** Date: 01/25/2019
 ** Description: This program is a game of zoo tycoon
 *********************************************************************/

#ifndef PROJECT2_TURTLE_HPP
#define PROJECT2_TURTLE_HPP

#include "animal.hpp"

//turtle is a subclass of animal
class turtle : public animal {
public:
    //default constructor
    turtle() = default;

    //destructor
    ~turtle() = default;

    //constructor
    turtle(turtle *turtle);

    //set functions
    void setAnimalAge(int);

    //get functions
    int getAnimalCost();

    int getAnimalAge();

private:
    int age = 1;
    int cost = 100;
    int numberOfBabies = 10;
    int baseFoodCost = 5;
    int payoff = 5;
};


#endif //PROJECT2_TURTLE_HPP
